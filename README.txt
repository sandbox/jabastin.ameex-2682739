CONTENTS OF THIS FILE
---------------------
1) Introduction
2) Requirements
3) Installation
4) Configuration

INTRODUCTION
------------

The youtube play channel module will display the entire youtube channel videos
in a block. When user click on the thumbnail then the video will play.

REQUIREMENTS
------------

This module requires the following module:

 * Block (Core module in drupal 8)

 INSTALLATION
 ------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/documentation/install/modules-themes/modules-8
   for further informations.

 CONFIGURATION
 -------------

 * Configure admin form in admin/config under development click
   youtube play link:

   - API Key

   		Your youtube API key
   		Create your own API key using the following link.
   		(https://console.developers.google.com)

   - Play Id

   		Your youtube channel ID.

   - Thumbnail Title

   		If user want thumbnail title then select yes. Otherwise select No.

   - Thumbnail Description

   		If user want thumbnail Description then select yes. Otherwise select No.

   - Video limit

   		It will display number of video thumbnails in front of your block.

   - Width and Height

   		User can set the width and height of youtube video player.
