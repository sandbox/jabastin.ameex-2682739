/**
 * @file
 * Click function to play a youtube channel video.
 */

(function ($) {
  'use strict';
  var youtubeid;
  function youtubeplay_setvideo(href) {
    youtubeid = href.replace('#', '');
    jQuery('#youtubeplay-frame').attr('src', 'http://www.youtube.com/embed/' + youtubeid);
  }

  Drupal.behaviors.youtubechannel = {
    attach: function (context, settings) {
      if (jQuery('#youtubeplay-list a:first').length) {
        youtubeplay_setvideo(jQuery('#youtubeplay-list a:first').attr('href'));
        jQuery('#youtubeplay-list a').click(function (e) {
          youtubeplay_setvideo(jQuery(this).attr('href'));
          return false;
        });
      }
    }
  };
}(jQuery));
