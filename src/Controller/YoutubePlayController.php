<?php

namespace Drupal\youtube_play_channel\Controller;

use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\Exception\RequestException;

/**
 * Default controller for the youtube_play_channel module.
 */
class YoutubePlayController extends ControllerBase {
  /**
   * Function to reload previous page.
   */
  public function loadPage() {
    $request = \Drupal::request();
    return $request->server->get('HTTP_REFERER');
  }

  /**
   * Function to clear acquia varnish cache.
   */
  public function youtubeplayvideo() {
    $api_key = \Drupal::config('youtube_play_channel.settings')->get('youtube_play_channel_api');
    $youtube_id = \Drupal::config('youtube_play_channel.settings')->get('youtube_play_channel_id');
    $max_results = \Drupal::config('youtube_play_channel.settings')->get('youtube_play_channel_limit');
    // First, let's fetch the play feed to get the upload playlist.
    $url  = "https://www.googleapis.com/youtube/v3/channels?part=contentDetails&id=$youtube_id&maxResults=$max_results&fields=pageInfo/totalResults,items/contentDetails/relatedPlaylists/uploads&key=$api_key";
    try {
      $response = \Drupal::httpClient()->request('GET', $url, array('headers' => array(), 'http_errors' => FALSE));
    }
    catch (RequestException $e) {
    }
    catch (BadResponseException $e) {
    }
    catch (\Exception $e) {
    }
    $data = $response->getBody();
    $data = json_decode($data);
    $uploads_id = $data->items[0]->contentDetails->relatedPlaylists->uploads;
    if (!empty($data->items)) {
      $path = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&}&maxResults={$max_results}&fields=pageInfo/totalResults,items/snippet(resourceId/videoId,title,thumbnails/default/url,description)&playlistId={$uploads_id}&key={$api_key}";
      try {
        $response1 = \Drupal::httpClient()->request('GET', $path, array('headers' => array(), 'http_errors' => FALSE));
      }
      catch (RequestException $e) {
      }
      catch (BadResponseException $e) {
      }
      catch (\Exception $e) {
      }
      $playlistjson = (string) $response1->getBody();
      $feed_array = json_decode($playlistjson);

      // First, let's look in the feed and check there are some videos.
      if ($feed_array->pageInfo->totalResults == 0) {
        return t("There are no videos available on this channel.");
      }
      // Now let's iterate through our items.
      $videos = array();
      $desc = array();
      foreach ($feed_array->items as $value) {
        $youtube_id = $value->snippet->resourceId->videoId;
        $title_trim = \Drupal::config('youtube_play_channel.settings')->get('youtube_play_channel_titletrim');
        $full_title = $value->snippet->title;
        $title = substr($full_title, 0, $title_trim);
        $desc_trim = \Drupal::config('youtube_play_channel.settings')->get('youtube_play_channel_desctrim');
        $full_description = $value->snippet->description;
        if (isset($full_description)) {
          $description = substr($full_description, 0, $desc_trim);
        }
        $desc = array(
          'title' => $title,
          'thumb' => $value->snippet->thumbnails->default->url,
          'description' => $description,
        );
        $videos[$youtube_id] = $desc;
      }
      $vars['width'] = \Drupal::config('youtube_play_channel.settings')->get('youtube_play_channel_width');
      $vars['height'] = \Drupal::config('youtube_play_channel.settings')->get('youtube_play_channel_height');
      $vars['thumb_title'] = \Drupal::config('youtube_play_channel.settings')->get('youtube_play_channel_title');
      $vars['thumb_desc'] = \Drupal::config('youtube_play_channel.settings')->get('youtube_play_channel_desc');
      $vars['content'] = $videos;
      return $vars;
    }
  }

}
