<?php

namespace Drupal\youtube_play_channel\Form;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Configure custom settings for this site.
 */
class CustomForm extends ConfigFormBase {
  /**
   * Constructor for ComproCustomForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'youtube_play_channel_admin_form';
  }
  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['config.youtube_play_channel'];
  }
  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['youtubeplay'] = array(
      '#type' => 'fieldset',
      '#title' => t('Youtube channel settings'),
      '#collapsible' => FALSE,
    );

    $form['youtubeplay']['youtubeplay_api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Youtube Google API Key'),
      '#size' => 40,
      '#default_value' => $this->config('youtube_play_channel.settings')->get('youtube_play_channel_api'),
      '#required' => TRUE,
      '#description' => t('Your YouTube Google API key from your developer console.'),
    );

    $form['youtubeplay']['youtubeplay_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Youtube channel Play ID'),
      '#size' => 40,
      '#default_value' => $this->config('youtube_play_channel.settings')->get('youtube_play_channel_id'),
      '#required' => TRUE,
      '#description' => t('The youtube play ID you want to get the videos.'),
    );

    $form['youtubeplay']['youtubeplay_title'] = array(
      '#type' => 'select',
      '#title' => t('Thumbnail Title'),
      '#options' => array(
        'No' => t('No'),
        'Yes' => t('Yes'),
      ),
      '#default_value' => $this->config('youtube_play_channel.settings')->get('youtube_play_channel_title'),
      '#description' => t('Set this to <em>Yes</em> if you would like this category to be selected by default.'),
    );

    $form['youtubeplay']['title_trim'] = array(
      '#type' => 'textfield',
      '#title' => t('Number of title character to display in title'),
      '#size' => 40,
      '#default_value' => $this->config('youtube_play_channel.settings')->get('youtube_play_channel_titletrim'),
      '#states' => array(
        'visible' => array(
          ':input[name="youtubeplay_title"]' => array('value' => 'Yes'),
        ),
      ),
    );

    $form['youtubeplay']['youtubeplay_description'] = array(
      '#type' => 'select',
      '#title' => t('Thumbnail Description'),
      '#options' => array(
        'No' => t('No'),
        'Yes' => t('Yes'),
      ),
      '#default_value' => $this->config('youtube_play_channel.settings')->get('youtube_play_channel_desc'),
      '#description' => t('Set this to <em>Yes</em> if you would like this category to be selected by default.'),
    );

    $form['youtubeplay']['desc_trim'] = array(
      '#type' => 'textfield',
      '#title' => t('Number of description character to display'),
      '#size' => 40,
      '#default_value' => $this->config('youtube_play_channel.settings')->get('youtube_play_channel_desctrim'),
      '#states' => array(
        'visible' => array(
          ':input[name="youtubeplay_description"]' => array('value' => 'Yes'),
        ),
      ),
    );

    $form['youtubeplay']['youtubeplay_video_limit'] = array(
      '#type' => 'textfield',
      '#title' => t('Youtube channel video limit'),
      '#size' => 40,
      '#default_value' => $this->config('youtube_play_channel.settings')->get('youtube_play_channel_limit'),
      '#required' => TRUE,
      '#description' => t('Number of videos to be shown from youtube play (max 50)'),
    );

    $form['youtubeplay']['youtubeplay_video_width'] = array(
      '#type' => 'textfield',
      '#title' => t('Youtube channel video width'),
      '#size' => 40,
      '#default_value' => $this->config('youtube_play_channel.settings')->get('youtube_play_channel_width'),
      '#required' => TRUE,
      '#description' => t('Max width to youtube video. In px'),
    );

    $form['youtubeplay']['youtubeplay_video_height'] = array(
      '#type' => 'textfield',
      '#title' => t('Youtube channel video height'),
      '#size' => 40,
      '#default_value' => $this->config('youtube_play_channel.settings')->get('youtube_play_channel_height'),
      '#required' => TRUE,
      '#description' => t('Max height to youtube video. In px'),
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * Youtube API credentials form validate.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $api_key = $form_state->getValue('youtubeplay_api_key');
    $play_id = $form_state->getValue('youtubeplay_id');
    $title_display = $form_state->getValue('youtubeplay_title');
    $title_trim = $form_state->getValue('title_trim');
    $desc = $form_state->getValue('youtubeplay_description');
    $desc_trim = $form_state->getValue('desc_trim');
    $video_limit = $form_state->getValue('youtubeplay_video_limit');
    $video_width = $form_state->getValue('youtubeplay_video_width');
    $video_height = $form_state->getValue('youtubeplay_video_height');
    if (isset($api_key) && isset($play_id) && isset($video_limit) && isset($video_width) && isset($video_height)) {
      $this->configFactory()->getEditable('youtube_play_channel.settings')->set('youtube_play_channel_api', $api_key)->save();
      $this->configFactory()->getEditable('youtube_play_channel.settings')->set('youtube_play_channel_id', $play_id)->save();
      $this->configFactory()->getEditable('youtube_play_channel.settings')->set('youtube_play_channel_limit', $video_limit)->save();
      $this->configFactory()->getEditable('youtube_play_channel.settings')->set('youtube_play_channel_width', $video_width)->save();
      $this->configFactory()->getEditable('youtube_play_channel.settings')->set('youtube_play_channel_height', $video_height)->save();
      $this->configFactory()->getEditable('youtube_play_channel.settings')->set('youtube_play_channel_title', $title_display)->save();
      $this->configFactory()->getEditable('youtube_play_channel.settings')->set('youtube_play_channel_titletrim', $title_trim)->save();
      $this->configFactory()->getEditable('youtube_play_channel.settings')->set('youtube_play_channel_desc', $desc)->save();
      $this->configFactory()->getEditable('youtube_play_channel.settings')->set('youtube_play_channel_desctrim', $desc_trim)->save();
      drupal_set_message(t('Youtube video API credentials saved successfully'));
    }
  }

}
