<?php

namespace Drupal\youtube_play_channel\Plugin\Block;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Example: uppercase this please' block.
 *
 * @Block(
 *   id = "YoutubeBlock",
 *   admin_label = @Translation("Youtube Channel video block")
 * )
 */
class YoutubeBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      '#theme' => 'YoutubeBlock',
    );
  }

}
